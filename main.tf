provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example" {
  name     = "myResourceGroup"
  location = "eastus"
}

resource "azurerm_virtual_network" "example" {
  name                = "myVNet"
  location            = "eastus"
  resource_group_name = azurerm_resource_group.example.name
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "example" {
  name                 = "mySubnet"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_public_ip" "example" {
  name                = "myPublicIP"
  resource_group_name = azurerm_resource_group.example.name
  location            = "eastus"
  allocation_method   = "Static"
  sku                 = "Basic"
  sku_tier            = "Regional"
}

resource "azurerm_network_security_group" "example" {
  name                = "myNetworkSecurityGroup"
  location            = "eastus"
  resource_group_name = azurerm_resource_group.example.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTP"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface" "example" {
  name                = "myNIC"
  resource_group_name = azurerm_resource_group.example.name
  location            = "eastus"

  ip_configuration {
    name                          = "myNicConfiguration"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "example" {
  name                = "myVM"
  resource_group_name = azurerm_resource_group.example.name
  location            = "eastus"
  size                = "Standard_DS1_v2"

  computer_name                   = "myvm"
  admin_username                  = "azureuser"
  disable_password_authentication = true
  network_interface_ids           = [azurerm_network_interface.example.id]  # Add this line

  os_disk {
    caching                 = "ReadWrite"
    storage_account_type    = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  admin_ssh_key {
    username  = "azureuser"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDJoRxCrwdhl8D3HgDTAGoWGlK+coM934A8BE8cHoVS3dqV9VOFgUshhK55Aj5IZBuigRFmEORBrfd1VQ3DmqjNRJlt9v7uiq7FsK1EmJF8nHqj/LznZ6rkgKLL8CCyKvsJBt8pJrpCPI7sjux73RizclepccacDZYG9XfYhJ+WW1Hnsu7ygiIl/UVYIGuTiyCYz27OKAg7WeyCpihhcS/tY7BXnu/EC/ndooD/UA2YFbvj2FZQJ8UYedwZkS/kRDdGJ23AI8lPyQlHsP4zkxPJ1iZ0yqhI+n+4jAtyfUjq5xIznCOvGcsRDBzIY92x3/HyonDu/zczwiyeAQUGOEG9keKT5zaNSmc1iI3W62GVgZlZqlNUfFNCc+12oYcTR4wDZUQD3pnBDJButzwCTdkrHbw88KzzElf6uqJ4Vp4TTN8W5Iv/8uNXinTdqZRswMW6i0lHzvvC1Jhvf1k1W9XhOO0XT3F2quAod69W1bCjLq5RpVf8Lzl928S/2W3QShW6PUHuLJfqMnJCcPS9RwjBod5dRKIQwtbTclC9Mzs8TVw8bl0/gBqilSacvJsgrkfIB9MDvjjebosNjLA8FD0snVMDxUs0eisPWva+rYTrjOmxPIVy2Pf+KLIZ2atziMRHaRE6NCeyu5/uEOPcd6Mu+GohichaXn0PNfIcMdZ8w== netanelyad2@gmail.com"
  }

  tags = {
    environment = "test"
  }
}

resource "azurerm_postgresql_server" "example" {
  name                = "postgresqlservernetanel"
  location            = "eastus"
  resource_group_name = azurerm_resource_group.example.name
  sku_name            = "GP_Gen5_2"
  version             = "10"
  administrator_login          = "postgres"
  administrator_login_password = "Netanel123"
  ssl_enforcement_enabled     = true
  ssl_minimal_tls_version_enforced = "TLS1_2"
  backup_retention_days = 7
  geo_redundant_backup_enabled = false
  auto_grow_enabled = true
  storage_mb = 5120
  create_mode = "Default"
  public_network_access_enabled = true

  tags = {
    environment = "test"
  }
}
